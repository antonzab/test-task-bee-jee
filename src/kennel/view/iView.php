<?php


namespace kennel\view;

interface iView
{
    function template(string $view, array $params);
    public static function getInstance();

}