<?php

namespace services\Traits;

trait Guarder
{
    private function guarderXSS($request){
        return array_map(function($value){
            return  htmlentities($value, ENT_QUOTES, "UTF-8");
        }, $request);
    }
}