<?php

namespace services\User\validator;

use services\Traits\Guarder;

class UserValidator implements IValidator
{

    use Guarder;

    private $validFields;
    private $failFields;
    private $request;

    public function __construct($request)
    {
        $this->validFields = [];
        $this->request = $this->guarderXSS($request);
    }

    public function validate(): bool
    {
        if ($this->validateLogin()) {
            $this->validFields['login'] = $this->request['login'];
        } else {
            $this->failFields['login'] = $this->request['login'];
        }

        if ($this->validatePassword()) {
            $this->validFields['password'] = $this->request['password'];
        } else {
            $this->failFields['password'] = $this->request['password'];
        }

        return empty($this->failFields);
    }

    private function validateLogin()
    {
        return (
            !empty($this->request['login']) and
            strlen($this->request['login']) > 2 and
            strlen($this->request['login']) < 255
        );
    }


    private function validatePassword()
    {
        return (
            !empty($this->request['password']) and
            strlen($this->request['password']) > 2 and
            strlen($this->request['password']) < 65535
        );
    }

    public function validFields(): array
    {
        return $this->validFields;
    }

    public function failFields(): array
    {
        return $this->failFields;
    }

}