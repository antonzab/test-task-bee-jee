@extends('layout.task')

@section('content')
    <div class="container">
        @include('messages.messages')
        @include('task.sorting')
        @include('task.list')
        @include('task.pagination')
        @include('task.form')
    </div>
@endsection