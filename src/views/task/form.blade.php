<form action="/task/add" method="POST" class="taskform">
    <div class="mb-3">
        <label class="form-label">Имя пользователя </label>
        <input type="text" name="user_name" required class="form-control"/>
    </div>
    <div class="mb-3">
        <label class="form-label">E-mail</label>
        <input type="email" name="email" required class="form-control"/>
    </div>
    <div class="mb-3">
        <label class="form-label">Текст задачи</label>
        <textarea name="text" class="form-control"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Отправить</button>
</form>