<?php

namespace kennel\services;

use Illuminate\Database\Capsule\Manager;

class Database
{
    function __construct()
    {
        $capsule = new Manager();
        $capsule->addConnection([
            "driver" => $_ENV['DB_CONNECTION'],
            "host" => $_ENV['DB_HOST'],
            "database" => $_ENV['DB_DATABASE'],
            "username" => $_ENV['DB_USERNAME'],
            "password" => $_ENV['DB_PASSWORD'],
            "charset" => "utf8",
            "port" => $_ENV['DB_PORT'],
            "collation" => "utf8_unicode_ci",
            "prefix" => "",
        ]);

        $capsule->bootEloquent();
    }
}