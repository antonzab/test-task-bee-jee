<?php

namespace services\Pagination;

use Illuminate\Database\Eloquent\Model;

interface IPaggination
{
    public function __construct(Model $model);
    function getPages():array;
}