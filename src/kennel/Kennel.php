<?php


namespace kennel;

use Dotenv\Dotenv;
use kennel\services\Database;
use kennel\session\Session;

class Kennel implements IKennel
{
    private $dotenv;
    public function __construct()
    {
        $this->dotenv = Dotenv::createImmutable(__DIR__.'/../');
    }

    public function init():void {
        Session::getInstance();
        $this->dotenv->load();
        new Database();
    }
}