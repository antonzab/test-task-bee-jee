<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Task</title>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet"/>
    <script type="text/javascript" src="/js/task.js"></script>
</head>
<body>
<div class="topmenu">
    <a href="/">Задачи</a>
    @if(\kennel\Auth::admin())
        <a href="/logout">Выйти</a>
    @else
        <a href="/login">Войти</a>
    @endif
</div>
@yield('content')

</body>
</html>