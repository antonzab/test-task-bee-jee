@extends('layout.task')

@section('content')
    <div class="container">
        @include('messages.messages')
        @if(!$user)
            <form action="/auth" method="post" class="loginform">
                <div class="mb-3">
                    <label class="form-label">Логин</label>
                    <input type="text" class="form-control" name="login"/>
                </div>
                <div class="mb-3">
                    <label class="form-label">Пароль</label>
                    <input type="password" name="password" class="form-control"/>
                </div>
                <button type="submit" class="btn btn-primary">Войти</button>
            </form>
        @else
            Вы уже авторизованы! <a href="/logout">Выйти?</a>
        @endif
    </div>
@endsection