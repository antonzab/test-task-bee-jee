<?php


namespace controllers;


use kennel\session\Session;
use kennel\view\View;
use services\User\UserService;

class AuthController
{
    function index() {
        $user = !empty($_SESSION['user'])?$_SESSION['user']:false;
        return View::getInstance()->template('auth.login', [
            'message' => Session::getInstance()->getFlash(),
            'user' => $user
        ]);
    }

    function login() {
        $userService = new UserService();
        $user = $userService->login($_POST);
        if(!empty($user->id)) {
            $_SESSION['user'] = $user;
            return header('Location: /');
        }
        else {
            Session::getInstance()->addFlash(['message'=>'Введенные данные не верны!']);
        }
        return header('Location: /login');
    }

    function logout() {
        unset($_SESSION['user']);
        return header('Location: /login');
    }
}