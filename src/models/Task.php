<?php

namespace models;

class Task extends \Illuminate\Database\Eloquent\Model
{
    protected $fillable = [
        'user_name','email','text', 'complected'
    ];

    public function getEditedAttribute() {
        return $this->updated_at->timestamp != $this->created_at->timestamp;
    }
}