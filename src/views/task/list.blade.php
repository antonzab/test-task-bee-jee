@foreach($tasks as $task)
    <div class="task" id="task_{{$task->id}}">
        @if($task->edited)
            Отредактированно Администратором
        @endif
        <div>
            Текст: {!! $task->text !!}
        </div>
        <div>
            Email: {{$task->email}}
        </div>
        <div>
            Имя пользователя: {{$task->user_name}}
        </div>
        <div>
            Статус:
            @if($task->complected)
                Выполнена
            @else
                Не выполнена
            @endif
        </div>
        @if($user)
            <div class="updateForm">
                @include('task.admin.form_update')
            </div>
            <button class="btn btn-success" onclick="editTask({{$task->id}})">Редактировать</button>
        @endif
    </div>
@endforeach