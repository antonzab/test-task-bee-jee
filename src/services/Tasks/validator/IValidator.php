<?php


namespace services\Tasks\validator;


interface IValidator
{
    public function __construct($request);
    public function validate():bool;
    public function validFields():array;
    public function failFields():array;
}