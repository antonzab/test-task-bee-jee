<?php

namespace kennel\services;

class Router
{
    private static $routes = array();
    public static function route(string $path, string $callback)
    {
        self::$routes[$path] = $callback;
    }

    private $url;

    function __construct(string $url)
    {
        $this->url = $url;
    }

    public function execute(): void
    {
        $url = parse_url($this->url);
        foreach (self::$routes as $path => $callback) {
            if ($url['path'] == $path)
            {
                $this->callController($callback);
            }
        }
    }

    private function callController(string $controller):void {
        $explode = explode('@', $controller);
        $controller = 'controllers\\'.$explode[0];
        $method = $explode[1];
        (new $controller())->$method();
    }
}