<?php


namespace kennel\session;

interface iSession
{
    function addFlash(array $params);
    function getFlash();
    public static function getInstance();

}