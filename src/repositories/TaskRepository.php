<?php

namespace repositories;

use models\Task;

class TaskRepository extends RepositoryAbstract
{
    protected function setModel()
    {
        $this->model = Task::class;
    }

    public function paginate($page, $sort = false)
    {

        $limit = $_ENV['PAGINATION_LIMIT'];
        $offset = ($page - 1) * $limit;

        $task = $this->instance()->select('id', 'user_name', 'email', 'text', 'complected', 'created_at', 'updated_at')
            ->offset($offset)
            ->limit($limit);

        if ($sort) {
            $task->orderByDesc($sort);
        }

        return $task->get();
    }

    public function getTask($taskId)
    {
        return $this->instance()->find($taskId);
    }
}