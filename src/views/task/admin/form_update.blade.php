<form action="/task/update" method="POST" class="taskform">
    <div class="mb-3">
        <label class="form-label">Текст задачи</label>
        <textarea name="text" class="form-control">{{$task->text}}</textarea>
    </div>
    <div class="form-check">
        <label>
            <input class="form-check-input"
                   type="checkbox"
                   name="complected"
                   @if($task->complected) checked="checked" @endif
            /> Выполнена
        </label>
    </div>
    <input type="hidden" name="task_id" value="{{$task->id}}"/>
    <button type="submit" class="btn btn-primary">Отправить</button>
</form>