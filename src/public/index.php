<?php

use kennel\Router;

require_once __DIR__ . '/../vendor/autoload.php';

error_reporting(E_ALL);

require_once __DIR__ . '/../router/routes.php';

$kennel = new \kennel\Kennel();
$kennel->init();
(new \kennel\services\Router($_SERVER['REQUEST_URI']))->execute();
