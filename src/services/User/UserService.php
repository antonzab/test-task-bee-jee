<?php

namespace services\User;

use models\User;
use services\User\validator\UserValidator;

class UserService
{
    function login($request) {
        $userValidator = new UserValidator($request);
        if($userValidator->validate()) {
            $validateFields = $userValidator->validFields();
            return $this->auth($validateFields['login'], $validateFields['password']);
        }
        else {
            return $userValidator->failFields();
        }
    }

    private function auth(string $login, string $password) {
        $users = User::where('login', $login)->get();
        foreach ($users as $user) {
            if (password_verify($password, $user->password)) {
                return $user;
            }
        }
    }
}