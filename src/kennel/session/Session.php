<?php


namespace kennel\session;


class Session implements iSession
{
    private static $instances = [];

    protected function __construct()
    {
        session_start();
    }

    protected function __clone(){}

    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    public static function getInstance()
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new Session();
        }

        return self::$instances[$cls];
    }


    function addFlash(array $params)
    {
        $_SESSION['flash'] = $params;
    }

    function getFlash()
    {
        if(empty($_SESSION['flash']))
            return false;

        $flash =  $_SESSION['flash'];
        unset($_SESSION['flash']);
        return $flash;
    }
}