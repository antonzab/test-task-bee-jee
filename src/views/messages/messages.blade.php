@if($message)
    <div class="alert alert-primary" role="alert">{{$message['message']}}</div>
@endif
@isset($message['error'])
    @foreach($message['error'] as $field=>$value)
        <div class="alert alert-danger" role="alert">
            Поле  {{$field}}  имеет недопустимое значение
        </div>
    @endforeach
@endisset
