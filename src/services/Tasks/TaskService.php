<?php

namespace services\Tasks;

use models\Task;
use services\Tasks\validator\TaskValidator;

class TaskService
{
    public function create($request)
    {
        $validator = new TaskValidator($request);
        if ($validator->validate()) {
            return Task::create($validator->validFields());
        }
        return $validator->failFields();
    }

    public function update($request)
    {
        $validator = new TaskValidator($request);
        if ($validator->updateValidate()) {
            return Task::where('id', $request['task_id'])
                ->update($validator->validFields());
        }
        return $validator->failFields();
    }
}