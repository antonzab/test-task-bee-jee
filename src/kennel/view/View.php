<?php


namespace kennel\view;


use Jenssegers\Blade\Blade;

class View implements iView
{
    private static $instances = [];
    private $blade;

    protected function __construct()
    {
        $views = __DIR__.'/../../views';
        $cache = __DIR__.'/../../cache';
        $this->blade = new Blade($views, $cache);
    }

    protected function __clone(){}

    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    public static function getInstance()
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new View();
        }

        return self::$instances[$cls];
    }


    function template(string $view, $params = [])
    {
        echo $this->blade->make($view, $params)->render();
    }
}