<?php

namespace services\Pagination;

use Illuminate\Database\Eloquent\Model;

class PaginationService implements  IPaggination
{
    private $model;
    private $pageLimit;

    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->pageLimit = $_ENV['PAGINATION_LIMIT'];
    }

    function getPages(): array
    {
        $count =  $this->model->count();
        $maxPages = ceil($count/$this->pageLimit);
        $pages = [];
        for($i = 1; $i<=$maxPages; $i++) {
            $pages[] = $i;
        }
        return $pages;
    }

}