<?php


namespace kennel;


class Auth implements IAuth
{
    static function admin():bool {
        return !empty($_SESSION['user']);
    }
}