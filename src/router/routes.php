<?php

use kennel\services\Router;

Router::route('/', 'TaskController@index');
Router::route('/task/add', 'TaskController@add');
Router::route('/login', 'AuthController@index');
Router::route('/auth', 'AuthController@login');
Router::route('/logout', 'AuthController@logout');
Router::route('/task/update', 'TaskController@update');
