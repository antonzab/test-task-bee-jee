<?php


namespace repositories;


abstract class RepositoryAbstract
{
    protected $model;

    public function __construct()
    {
        $this->setModel();
    }

    abstract protected function setModel();

    public function instance()
    {
        return new $this->model;
    }

}
