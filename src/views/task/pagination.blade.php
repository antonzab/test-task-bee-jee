@if(count($pages) > 7)
    <div class="pagination">
        <span>Страницы:</span>
        @foreach($pages as $page)
            @php
                $class = ($page == $currentPage)?'class=active':'';
            @endphp
            @if($sort)
                <a {{$class}} href="?page={{$page}}&sort={{$sort}}">{{$page}}</a>
            @else
                <a {{$class}} href="?page={{$page}}">{{$page}}</a>
            @endif
        @endforeach
    </div>
@endif