<?php

namespace services\Tasks\validator;

use services\Traits\Guarder;

class TaskValidator implements IValidator
{

    use Guarder;
    private $validFields;
    private $failFields;
    private $request;

    public function __construct($request)
    {
        $this->validFields = [];
        $this->request = $this->guarderXSS($request);
    }



    public function validate(): bool
    {
        if ($this->validateName()) {
            $this->validFields['user_name'] = $this->request['user_name'];
        } else {
            $this->failFields['user_name'] = $this->request['user_name'];
        }

        if ($this->validateEmail()) {
            $this->validFields['email'] = $this->request['email'];
        } else {
            $this->failFields['email'] = $this->request['email'];
        }

        if ($this->validateText()) {
            $this->validFields['text'] = $this->request['text'];
        } else {
            $this->failFields['text'] = $this->request['text'];
        }

        return empty($this->failFields);
    }

    public function updateValidate() {

        if ($this->validateText()) {
            $this->validFields['text'] = $this->request['text'];
        } else {
            $this->failFields['text'] = $this->request['text'];
        }
        $this->validFields['complected'] = $this->validateComplected();
        return empty($this->failFields);
    }

    private function validateComplected() {
        return !empty($this->request['complected']) && $this->request['complected'] == 'on';
    }

    private function validateName()
    {
        return (
            !empty($this->request['user_name']) and
            strlen($this->request['user_name']) > 2 and
            strlen($this->request['user_name']) < 255
        );
    }

    private function validateEmail()
    {
        return (filter_var($this->request['email'], FILTER_VALIDATE_EMAIL));
    }

    private function validateText()
    {
        return (
            !empty($this->request['text']) and
            strlen($this->request['text']) > 2 and
            strlen($this->request['text']) < 65535
        );
    }

    public function validFields(): array
    {
        return $this->validFields;
    }

    public function failFields(): array
    {
        return $this->failFields;
    }

}