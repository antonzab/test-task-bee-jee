<?php


namespace kennel;


interface IKennel
{
    public function init():void;
}