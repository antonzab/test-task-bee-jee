<?php

namespace controllers;

use kennel\Auth;
use kennel\session\Session;
use kennel\view\View;
use models\Task;
use repositories\TaskRepository;
use services\Pagination\PaginationService;
use services\Tasks\TaskService;

class TaskController
{
    public function index()
    {
        $taskRepository = new TaskRepository();
        $page = !empty($_GET['page']) ? $_GET['page'] : 1;
        $sort = !empty($_GET['sort']) ? $_GET['sort'] : false;
        $tasks = $taskRepository->paginate($page, $sort);

        return View::getInstance()->template('task.index', [
            'message' => Session::getInstance()->getFlash(),
            'sort' => $sort,
            'tasks' => $tasks,
            'user' => !empty($_SESSION['user']) ? $_SESSION['user'] : false,
            'pages' => (new PaginationService(new Task))->getPages(),
            'currentPage' => !empty($_GET['page']) ? $_GET['page'] : 1,
        ]);
    }

    public function add()
    {
        $taskService = new TaskService();
        $result = $taskService->create($_POST);

        $flash = [];
        if (!empty($result->id)) {
            $flash['message'] = 'Задача ID#'.$result->id.' успешно добавлена ';
        } elseif (is_array($result)) {
            $flash['message'] = 'Задача не прошла валидацию!';
            $flash['error'] =  $result;
        } else {
            $flash['message'] = 'Что-то пошло не так!';
        }

        Session::getInstance()->addFlash($flash);

        return header('Location: /');
    }

    public function update()
    {
        if (!Auth::admin()) {
            Session::getInstance()->addFlash(['message' => 'Редактирование доступно только администратору!']);
            return header('Location: /login');

            return false;
        }

        $result = (new TaskService())->update($_POST);
        $flash = [];
        if ($result !== false) {
           $flash['message'] = 'Задача успешно обновлена';
        } elseif (is_array($result)) {
            $flash['message'] = 'Задача не прошла валидацию!';
            $flash['error'] = $result;
        } else {
            $flash['message'] = 'Что-то пошло не так!';
        }
        Session::getInstance()->addFlash($flash);

        return header('Location: /');
    }

}